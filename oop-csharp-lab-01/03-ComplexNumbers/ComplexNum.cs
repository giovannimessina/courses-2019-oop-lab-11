﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }

        public static ComplexNum Invert(this ComplexNum value)
        {
            return value.Conjugate / Math.Pow(value.Module,2);//modo per convertire primo operando in double
        }
        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(Math.Pow(this.Re, 2) + Math.Pow(this.Im, 2));
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(this.Re, - this.Im);
            }
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return $"{this.Re} + {this.Im}i";
        }

        public static ComplexNum operator +(ComplexNum var1, ComplexNum var2)
        {
            return new ComplexNum(var1.Re + var2.Re, var1.Im + var2.Im);
        }

        public static ComplexNum operator -(ComplexNum var1, ComplexNum var2)
        {
            return new ComplexNum(var1.Re - var2.Re, var1.Im - var2.Im);
        }

        public static ComplexNum operator *(ComplexNum var1, ComplexNum var2)
        {
            return new ComplexNum((var1.Re* var2.Re) - (var1.Im * var2.Im), (var1.Re * var2.Im) + (var1.Im * var2.Re));
        }

        public static ComplexNum operator /(ComplexNum var1, ComplexNum var2)
        {
            return new ComplexNum( ( (var1.Re * var2.Re) + (var1.Im * var2.Im) ) / new ComplexNum(var2.Re, var2.Im).Module , 
                                                                                ((var1.Im * var2.Re) - (var1.Re * var2.Im)) / new ComplexNum(var1.Re, var1.Im).Module );
        }

    }
}
