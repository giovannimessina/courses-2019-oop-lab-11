﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck : Deck
    {
        private Card[] cards;

        public FrenchDeck()
        {
            cards = new Card[Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length];
        }
        

        public Card this[FrenchSeed seed, FrenchValue value]
            {
            get
            {
                 foreach (Card c in cards)
                 {
                    if (c.Seed.ToString().Equals(seed.ToString()) && c.Value.ToString().Equals(value.ToString()))
                    {
                        return c;
                    }
                 }
                return null;
            }
               
            }

        public override void Initialize()
        {
            int i = 0;
            foreach (FrenchSeed s in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
            {
                foreach (FrenchValue v in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                {
                    if (v.ToString() == "JOLLY")
                    {
                        cards[i] = new Card(v.ToString(), "NONE");
                    }
                    else
                    {
                        cards[i] = new Card(v.ToString(), s.ToString());
                    }
                        i++;
                }
            }
        }


        public override void Print()
        {
           

            foreach (Card c in cards)
            {
                Console.WriteLine(c);
            }
        }
    }



    enum FrenchSeed
    {
        PICCHE,
        QUADRI,
        FIORI,
        CUORI,
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE,
        JOLLY
    }
}
