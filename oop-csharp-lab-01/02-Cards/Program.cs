﻿using System;

/*
 * == README ==
 * 
 * 1) Analizzare la classe Card e trasformare i metodi presenti in proprietà. V
 * 2) Comprendere il meccanismo di "string interpolation" presente nel metodo ToString() della classe Card. V
 * 3) La classe Deck rappresenta il mazzo di carte. Completare i metodi Initialize() e Print(). V
 * 4) Definire un indicizzatore sulla classe Deck in modo da poter ottenere una carta dal mazzo specificando seme e valore. V
 * 5) Qualora si volesse definire una nuova classe "FrenchDeck" che contempli un mazzo di carte francesi (semi: picche, quadri, fiori, cuori) V
 *    come protebbe essere modellato? Si noti che in tale mazzo sono presenti anche le  carte jolly, non appartenenti a nessun seme.
 *    Si poronga un'implementazione indipendentemente da quanto già esistente (ragionare su come poter implementare l'indicizzatore...)
 *    (si cerchi di evitare l'uso dei generici, oggetto della prossima lezione di teoria)
 */

namespace Cards
{
    class Program
    {
        static void Main(string[] args)
        {
            Deck deck = new Deck();
            FrenchDeck Fdeck = new FrenchDeck();

            Console.WriteLine("== Carte ITALIANE ==");
            deck.Initialize();
            deck.Print();

            Console.WriteLine("== Alcune Carte ==");
            
            Card assobastoni = deck[ItalianSeed.BASTONI, ItalianValue.ASSO];
            Console.WriteLine(assobastoni.ToString());


            Console.WriteLine("== Carte FRANCESI ==");
            Fdeck.Initialize();
            Fdeck.Print();

            Console.WriteLine("== Alcune Carte ==");

            Card fioriasso = Fdeck[FrenchSeed.FIORI, FrenchValue.ASSO];
            Console.WriteLine(fioriasso.ToString());

            Console.ReadKey();
        }
    }
}
